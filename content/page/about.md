---
title: About me
comments: false
---
![IMG_5949](https://user-images.githubusercontent.com/58679512/73321274-fb859600-420f-11ea-8eb9-03fec7ade011.JPG)

I currently work remotely as a data scientist in consulting, where I bring my experience in computational linguistics and natural language processing to build machine learning solutions for client needs. I have a Master's degree from Michigan State University in linguistics.  I also received my Bachelor's degree from MSU in viola performance with a minor in linguistics. My main areas of interest are document AI, information extraction, and speech/audio signal data.

### my work

In my most recent position I work on tools using neural nets, classical machine learning, and image processing to extract structured data from text, specifically contract documents. Additionally, I research the effects of different word vectorization techniques on machine learning models.
I am wrote my Master's thesis on phonotactic knowledge and neural nets; I modeled sounds and their ability to combine with others to rate acceptability of novel words in a given language. I am passionate about ways that machine learning models can be integrated with theoretical linguistics. 

As a research assistant in linguistics at MSU, I have also worked on the semantics of classifiers and numerals in Thai and phonological perception experiments. 

### other interests
When I'm not working on data science projects, I love playing viola and piano. I also love reading (my favorite author is Chaim Potok!), learning Spanish and Thai, and spending time outdoors.
