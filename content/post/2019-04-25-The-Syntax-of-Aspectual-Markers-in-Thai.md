---
layout: default
title: The Syntax of Aspectual Markers in Thai
date: 2019-04-25
---

In Thai, there are at least seventeen aspectual `markers', or words that mark aspect on the verb. These words can have multiple functions: some can act as a verb themselves, some cannot. These markers can also occur together but have a number of restrictions on how that can occur. Some can license or block others from occuring; some scope from left to right, and some scope from right to left. Some can appear before the verb, and some afterwards.

In this project, I will present preliminary data to introduce the Thai aspectual system and give a general idea of how the aspectual markers appear in the language. **Current analyses are incompatible based on their assumptions of homynymy: when an aspectual marker has two different functions in Thai, should it be considered the same word or two semantically or syntactically distinct words that are pronounced the same?**

I discuss the work in Chiravate (2002) and the case for unified definitions of these aspectual markers despite their different functions. Past analyses invoke cross-linguistic patterns for aspectual markers, so I present my own data from consultants in Arabic, Chinese, Thai, and Spanish, and discuss how my data compares to Chiravate's analysis. I then contrast this analysis of the aspectual system with the work in Koenig and Muansuwan (2005) and discuss how this analysis provides a partitioning of the markers into three different groups in order to justify a syntax and semantics that accounts for the scoping relations of these words, but relies on multiple definitions of some aspectual markers. You can read the full paper [here](https://isaacsarver.gitlab.io/post/ThaiAspectualMarkers.pdf). Please email me for corrections, questions, or comments!