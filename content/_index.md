## Hello everyone!

I'm Isaac, a data scientist based in Dallas, TX. On this website you can find my bio, CV, and some project descriptions detailing my recent work.
 
You can also follow me on [Github](https://github.com/isaacsarver) and [LinkedIn](https://linkedin.com/in/isaac-sarver).
